import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import 'normalize.css';

angular.module('app', [
    uiRouter,
    Common,
    Components
  ])
  .config(($locationProvider, $qProvider) => {
    "ngInject";
    // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
    // #how-to-configure-your-server-to-work-with-html5mode
    //$locationProvider.html5Mode(true).hashPrefix('!');

    $qProvider.errorOnUnhandledRejections(false);
  })
  .run(($state, $transitions, $window) => {
    "ngInject";

    $transitions.onEnter({}, function(transition) {
      $window.scrollTo(0, 0);
    });
  })

  .component('app', AppComponent);
