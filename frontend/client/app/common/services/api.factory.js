import moment from 'moment';

class API {
  constructor($http, $q, $state, localStorageService, Notification, config) {
    this.$http = $http;
    this.$q = $q;
    this.$state = $state;
    this.localStorageService = localStorageService;
    this.Notification = Notification;
    this.config = config;
  }

  displayError(data, status) {
    if (status === 403) {
      this.localStorageService.remove('auth');
      this.$state.go('auth.login');
    }
  }

  apiUrl(url) {
    const externalApiEndpoint = url.indexOf('http') > -1 && url.indexOf('=http') === -1;
    return (externalApiEndpoint ? url : this.config.apiUrl + url);
  }

  successCallback(response, doNotShowNotification) {
    if (response.data.message && !doNotShowNotification) {
      this.Notification.success({message: response.data.message, delay: 3000});
    }

    return response.data;
  }

  errorCallback(response, doNotShowNotification) {
    if (response.status === 403 && response.data.not_logged_in) {
      this.localStorageService.remove('auth');
      this.localStorageService.remove('meData');
      this.$state.go('auth.login');

      return;
    }

    if (!doNotShowNotification){
      if (response.status === 422) {
        for (let field in response.data.errors) {

          if (response.data.errors.hasOwnProperty(field)) {
            this.Notification.error({message: response.data.errors[ field ][0], delay: 3000});
          }
        }
      } else if (response.data.message) {
        this.Notification.error({message: response.data.message, delay: 3000});
      }
    }

    return this.$q.reject(response.data);
  }

  get(url, params, doNotShowNotification, headers) {
    let execute = () => {
      return this.$http(config).then((response) => {
        return this.successCallback(response, doNotShowNotification);
      }, (response) => {
        return this.errorCallback(response, doNotShowNotification);
      });
    };

    let config = {
      method: 'GET',
      url: this.apiUrl(url),
      headers: headers || {'Content-Type': 'application/json'},
    };

    if (params) {
      config.params = params;

      if (params.ignoreLoadingBar) {
        config.ignoreLoadingBar = true;
      }
    }

    return execute();
  }

  post(url, data = {}, doNotShowNotification, headers) {
    let execute = () => {
      return this.$http(config).then((response) => {
        return this.successCallback(response, doNotShowNotification);
      }, (response) => {
        return this.errorCallback(response, doNotShowNotification);
      });
    };
      
    let config = {
      method: 'POST',
      url: this.apiUrl(url),
      headers: headers || {'Content-Type': 'application/json'},
      data: data ? JSON.stringify(data) : null
    };

    return execute();
  }

  put(url, data, doNotShowNotification) {
    let execute = () => {
      return this.$http(config).then((response) => {
        return this.successCallback(response, doNotShowNotification);
      }, (response) => {
        return this.errorCallback(response, doNotShowNotification);
      });
    };

    let config = {
      method: 'PUT',
      url: this.apiUrl(url),
      headers: {'Content-Type': 'application/json'},
      data: data ? JSON.stringify(data) : null
    };

    return execute();
  }

  delete(url, data, doNotShowNotification) {
    let config = {};

    if (data) {
      config.headers = {'Content-Type': 'application/json'};
      config.data = JSON.stringify(data);
    }

    return this.$http
      .delete( this.apiUrl(url), config )
      .then((response) => {
        if (doNotShowNotification) {
          this.successCallback(response, doNotShowNotification);
        } else {
          this.successCallback(response);
        }
      }, this.errorCallback);
  }

}

API.$inject = ['$http', '$q', '$state', 'localStorageService', 'Notification', 'config'];

export default API;