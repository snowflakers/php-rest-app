import angular from 'angular';
import ngMaterial from 'angular-material';
import localStorageService from 'angular-local-storage';
import angularUiNotification from 'angular-ui-notification';

import API from './services/api.factory';

import 'angular-material/angular-material.css';
import 'angular-ui-notification/dist/angular-ui-notification.css';

let commonModule = angular.module('app.common', [
  ngMaterial,
  localStorageService,
  angularUiNotification,
  
])

.config(($mdThemingProvider, NotificationProvider) => {
  "ngInject";

  $mdThemingProvider.theme('default')
    .primaryPalette('indigo')
    .accentPalette('blue');
})

.service({
  API
})

.constant('config', {apiUrl: process.env.API_URL || '/api/'})

.name;

export default commonModule;
