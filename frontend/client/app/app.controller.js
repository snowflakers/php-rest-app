class AppController {
  constructor($rootScope) {
    this.$rootScope = $rootScope;
  }
}

AppController.$inject = ['$rootScope'];

export default AppController;