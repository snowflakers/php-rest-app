class AddVideoController {
  constructor(API, $timeout) {
    this.API = API;
    this.data = new Video();
    this.$timeout = $timeout;
  }

  save() {
    return this.API.post('videos', this.data)
      .then(() => {
        this.initialisedList = false;

        this.$timeout(() => {
          this.initialisedList = true;
          this.data = new Video();
        }, 50);
      });
  }
}

class Video {
  constructor() {
    this.title = '';
    this.url = '';
  }
}

AddVideoController.$inject = ['API', '$timeout'];

export default AddVideoController;
