import template from './addVideo.html';
import controller from './addVideo.controller';
import './addVideo.scss';

let addVideoComponent = {
  bindings: {
    initialisedList: '='
  },
  template,
  controller,
  controllerAs: 'video'
};

export default addVideoComponent;
