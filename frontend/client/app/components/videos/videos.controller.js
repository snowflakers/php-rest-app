class VideosController {
  constructor(API) {
    this.API = API;
    this.videos = [];
    this.loading = true;
  }

  $onInit = () => {
    this.loadVideos();
  }

  loadVideos() {
    this.loading = true;

    return this.API.get('videos')
      .then((response) => {
        this.videos = response.list;
      })
      .finally(() => {
        this.loading = false;
      });
  }

  watch(url) {
    window.location.href = url;
  }

  delete(videoId, index) {
    return this.API.delete(`videos/${videoId}`)
      .then((response) => {
        this.videos.splice(index, 1);
      });
  }
}

VideosController.$inject = ['API'];

export default VideosController;
