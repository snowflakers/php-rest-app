import template from './videos.html';
import controller from './videos.controller';
import './videos.scss';

let videosComponent = {
  bindings: {
    initialisedList: '='
  },
  template,
  controller
};

export default videosComponent;
