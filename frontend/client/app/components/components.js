import angular from 'angular';
import Home from './home/home';
import AddVideo from './addVideo/addVideo';
import Videos from './videos/videos';

let componentModule = angular.module('app.components', [
  Home,
  AddVideo,
  Videos
])

.name;

export default componentModule;
