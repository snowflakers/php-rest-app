### Installation
- clone the repo
- in frontend/ folder, run ```npm install```
- in frontend/ folder, create .env file, using the frontend/.env.example file - there should be the API URL (it should be URL where the backend sits)
- run ```npm run watch``` to run the front-end application (based on Angular); open the application in your browser

### Task
Your task is to create simple RESTful API to add, display and remove videos. All videos should be saved in the database. 

- in folder backend/ install Laravel in latest LTS version; the backend should be available under the same name you have specified in frontend/.env file
- API should be accessible under /videos URL:
	- to create a video, request should go to POST /videos (please try to add some video in the front-end application to see how the payload should be handled on back-end side)
	- to display videos, request should go to GET /videos
	- to remove specific video, request should go to DELETE /videos/{videoId}
- in database, each video should have specific data saved:
	- title
	- description
	- url
	- YouTube Thumbnail Image URL
- API mocks (how the response) should look like, you can find in backend/ folder
- list of the videos should be cached
- additional points will be for the Repository Design Pattern used
